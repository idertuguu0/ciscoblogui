import React, { Component, Fragment } from "react";
import css from "./style.module.css";
import LoginPage from "../Page/LoginPage";
import Singup from "../Page/Singup";
import Toolbar from "../components/Toolbar";
import SideBar from "../components/SideBar";
import { Switch, Route } from "react-router-dom";
import HomePage from "../Page/Home";
import CreateNewPost from "../components/CreatNewPost";
import EditPost from "../components/EditPost";

//Cookie.set("auth", response.data.token, { path: "/home" });
class App extends Component {
  state = {
    showSidebar: false,
  };

  //const history = useHistory
  toggleSideBar = () => {
    this.setState((prevState) => {
      return { showSidebar: !prevState.showSidebar };
    });
  };
  render() {
    return (
      <div className={css.Desktop}>
        <Toolbar toggleSideBar={this.toggleSideBar} />
        <SideBar
          showSidebar={this.state.showSidebar}
          toggleSideBar={this.toggleSideBar}
        />
        <main>
          <Fragment>
            <Switch>
              <Route path="/signin/login" component={LoginPage} />
              <Route path="/signin" component={Singup} />
              <Route path="/post/create" component={CreateNewPost} />
              <Route path="/post/edit/:id" component={EditPost} />
              <Route path="/" component={HomePage} />
            </Switch>
          </Fragment>
          {/* <Button btnType="Success" text="Next" />
            <Button btnType="Danger" text="back" /> */}
        </main>
      </div>
    );
  }
}

export default App;
