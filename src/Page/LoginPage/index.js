import React, { Component } from "react";
import css from "./style.module.css";
import axios from "axios";
import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
    };
  }

  changeEmail = (e) => {
    let val = e.target.value;
    this.setState({ ...this.state, email: val });
  };

  changePassword = (e) => {
    let val = e.target.value;
    this.setState({ ...this.state, password: val });
  };

  logins = (e) => {
    e.preventDefault();
    const { history } = this.props;

    let request = {
      email: this.state.email,
      password: this.state.password,
    };
    console.log("request:", request);

    axios
      .post("/signin/login", request)
      .then((resp) => {
        localStorage.setItem("token", resp.data.token);
        localStorage.setItem("name", resp.data.name);
        localStorage.setItem("userid", resp.data.userId);
        history.push("/");
      })
      .catch((err) => {
        console.log("failed");
      });
  };

  render() {
    return (
      <div className={css.Login}>
        <h3>Login</h3>
        <form onSubmit={this.logins}>
          <input
            onChange={this.changeEmail}
            type="text"
            placeholder="Имэйл хаяг"
            value={this.state.email}
          />

          <input
            onChange={this.changePassword}
            type="password"
            placeholder="нууц үг"
            value={this.state.password}
          />
          <button type="submit">Нэвтрэх</button>
          <br />
          <Link to={"/signin"}>Бүртгүүлэх</Link>
        </form>
      </div>
    );
  }
}

export default withRouter(Login);
