import React, { Component } from "react";
import css from "./style.module.css";
import axios from "axios";

// const SingUp = ()=>{

// }
class SingUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      phone: "",
      password: "",
    };
  }

  changeName = (e) => {
    let val = e.target.value;
    this.setState({ ...this.state, name: val });
  };
  changeEmail = (e) => {
    let val = e.target.value;
    this.setState({ ...this.state, email: val });
  };
  changePhone = (e) => {
    let val = e.target.value;
    this.setState({ ...this.state, phone: val });
  };
  changePassword = (e) => {
    let val = e.target.value;
    this.setState({ ...this.state, password: val });
  };

  signin = (e) => {
    const { history } = this.props;
    e.preventDefault();
    let request = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
      phone: this.state.phone,
    };
    axios
      .post("/signin", request)
      .then((resp) => {
        console.log(resp.data.message);
        history.push("/signin/login");
      })
      .catch((err) => {
        //alert("failded");
        console.log("failed");
      });
  };

  render() {
    return (
      <div className={css.Singup}>
        <form onSubmit={(e) => this.signin(e)}>
          <h3>Та өөрийн мэдээллээ оруулна уу</h3>
          <input
            value={this.state.name}
            onChange={this.changeName}
            type="text"
            placeholder="Нэр"
          />
          <input
            value={this.state.email}
            onChange={this.changeEmail}
            type="text"
            placeholder="Имэйл хаяг"
          />
          <input
            value={this.state.phone}
            onChange={this.changePhone}
            type="number"
            placeholder="утас"
          />
          <input
            value={this.state.password}
            onChange={this.changePassword}
            type="password"
            placeholder="нууц үг"
          />
          <button type="submit">Хадгалах</button>
          {/* <button type="submit">Хадгалах</button> */}
        </form>
      </div>
    );
  }
}
export default SingUp;
