import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import css from "./style.module.css";
import Moment from "react-moment";
const userId = localStorage.getItem("userid");
class Posts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      postList: [],
    };
  }
  deletePost(_id) {
    axios.delete("/post/" + _id).then((response) => {
      this.componentDidMount();
    });
  }
  componentDidMount = () => {
    axios.get("/post").then((response) => {
      console.log('post list:', response.data)

      this.setState({
        postList: response.data,
      });
    });
  };

  render = () => {
    return (
      <section className={css.PostEdge}>
        <br />
        <div className={css.Color}>
          <Link className={css.Create} to={"/post/create"}>
            Create post
          </Link>
        </div>
        {this.state.postList.map((post) => {
          return (
            <div className={css.Space} key={post._id}>
              <div className={css.text}>
                <p className={css.name}>{post.userName} </p>
                <br />
                <h3> {post.title} </h3>
                <div>{post.post}</div>
                <div className={css.btndate}>
                  {post.userId === userId ? (
                    <Link
                      className={css.ButtonEdit}
                      to={`/post/edit/${post._id}`}
                    >
                      Edit
                    </Link>
                  ) : null}
                  {post.userId === userId ? (
                    <Link
                      className={css.ButtonDelete}
                      onClick={this.deletePost.bind(this, post._id)}
                    >
                      Delete
                    </Link>
                  ) : null}
                  <p className={css.Date}>
                    {<Moment format="YYYY/MM/DD HH:mm">{post.PostDate}</Moment>}
                  </p>
                </div>
              </div>
            </div>
          );
        })}
      </section>
    );
  };
}
export default Posts;
