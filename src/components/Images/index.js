import React from "react";
import css from "./style.module.css";
function Images() {
  const images = ["http://placeimg.com/1500/700/code"];
  return (
    <div className={css.Images}>
      <img className={css.Image} src={images} />
    </div>
  );
}
export default Images;
