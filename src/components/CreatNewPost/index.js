import React, { Component } from "react";
import { withRouter } from "react-router-dom";
import css from "./style.module.css";
import axios from "axios";

// new date time ---------
let PostTime;
PostTime = new Date();
//-------------------------

class CreateNewPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      post: "",
      PostDate: "",
      userId: "",
      userName: "",
    };
  }
  changeTitle = (e) => {
    let val = e.target.value;
    this.setState({ ...this.state, title: val });
  };

  changePost = (e) => {
    let val = e.target.value;
    this.setState({ ...this.state, post: val });
  };

  createPost = (e) => {
    const { history } = this.props;
    const token = localStorage.getItem("token");
    e.preventDefault();
    let request = {
      title: this.state.title,
      post: this.state.post,
      PostDate: PostTime.toString(),
      userId: localStorage.getItem("userid"),
      userName: localStorage.getItem("name"),
    };

    axios
      .post("/post", request, {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      })
      .then((resp) => {
        //alert(resp.data.message);
        console.log(resp.data.message);
        history.push("/");
      })
      .catch((err) => {
        //alert("failded");
        console.log("failed");
      });
  };

  onClick() {
    window.location.href = "https://tugsuublog.herokuapp.com/";
  }
  render() {
    return (
      <div className={css.CreatPost}>
        {console.log()}
        <form className={css.talbar} onSubmit={(e) => this.createPost(e)}>
          <h3>Create New Post</h3>
          <input
            className={css.title}
            onChange={this.changeTitle}
            type="text"
            placeholder="title"
            size="39"
            value={this.state.title}
          ></input>
          <br />
          <br />
          <textarea
            className={css.Post}
            onChange={this.changePost}
            placeholder="contents"
            rows="8"
            cols="41"
            required
            value={this.state.post}
          ></textarea>
          <br />
          <br />
          <button className={css.Button} type="submit">
            Хадгалах
          </button>
          <button className={css.ButtonBack} onClick={this.onClick}>
            Буцах
          </button>
        </form>
      </div>
    );
  }
}
export default withRouter(CreateNewPost);
