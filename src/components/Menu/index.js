import React from "react";
import { useHistory } from "react-router-dom";
import css from "./style.module.css";
import MenuItem from "../Menuitem";

const Menu = () => {
  const history = useHistory();

  const logout = (e) => {
    e.preventDefault();
    localStorage.removeItem("token");
    localStorage.removeItem("name");
    localStorage.removeItem("userid");
    history.push("/");
  };

  return (
    <div>
      <ul className={css.Menu}>
        <MenuItem link="/">Нүүр хуудас</MenuItem>
        <MenuItem link="/signin/login">нэвтрэх</MenuItem>
        {/* <MenuItem link="/signin/login" onClick={(e) => logout(e)}>
          Гарах
        </MenuItem> */}
        <li className={css.MenuItem}>
          <a href="/singin/login" onClick={(e) => logout(e)}>
            Гарах
          </a>
        </li>
      </ul>
    </div>
  );
};

export default Menu;
